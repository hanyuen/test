---
layout: page
title: Blue Cat Tote
date: 2018-04-29
image: /test/img/sewing/blue_cat/20180429_bluecat1.jpg 
categories: sewing

---
Features: Top zipper, front zipper pocket, side elastic pockets, lining

<h2>Materials</h2>
<img src="/test/img/sewing/blue_cat/20180429_bluecat1.jpg">
<br>
<img src="/test/img/sewing/blue_cat/20180429_bluecat2.jpg">
<img src="/test/img/sewing/blue_cat/20180429_bluecat3.jpg">

<h2>The finished tote</h2>
<img src="/test/img/sewing/blue_cat/20180429_bluecat1.jpg">
<br>
<img src="/test/img/sewing/blue_cat/line_1525701074084.jpg">

