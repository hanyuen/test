---
layout: page
title: Blue Tote
date: 2018-05-01
image: /test/img/sewing/blue_tote/20180426_bluetote.jpg 
categories: sewing

---
Features: Top zipper, front zipper pocket, side elastic pockets, lining



Cut all pieces
<img src="/test/img/sewing/blue_tote/20180421_allpieces.jpg">

Add fusible interfacing 
<img src="/test/img/sewing/blue_tote/20180422_interfacing.jpg">

<h2>Straps</h2>
<img src="/test/img/sewing/blue_tote/20180421_straps.jpg">

<h2>Side elastic pockets</h2>
<img src="/test/img/sewing/blue_tote/20180421_sidepockets.jpg">

<h2>Zipper panel</h2>
The zipper panel will be like this:
<img src="/test/img/sewing/blue_tote/20180422_zippanel1.jpg">

Line up two pieces facing each other at the edge like this and sew:
<img src="/test/img/sewing/blue_tote/20180422_zippanel2.jpg">

After sewing, flip over will look like this:
<img src="/test/img/sewing/blue_tote/20180422_zippanel3.jpg">

Do the same for the other side.
<img src="/test/img/sewing/blue_tote/20180422_zippanel7.jpg">

In order to sew the sides such that not to be seen, roll it like this:
<img src="/test/img/sewing/blue_tote/20180422_zippanel12.jpg">
<img src="/test/img/sewing/blue_tote/20180422_zippanel13.jpg">
<img src="/test/img/sewing/blue_tote/20180422_zippanel14.jpg">

Pull the zipper out:
<img src="/test/img/sewing/blue_tote/20180422_zippanel15.jpg">
<img src="/test/img/sewing/blue_tote/20180422_zippanel10.jpg">
<img src="/test/img/sewing/blue_tote/20180422_zippanel11.jpg">

Sew side seam all along:
<img src="/test/img/sewing/blue_tote/20180422_zippanel20.jpg">

Zipper tabs:
<img src="/test/img/sewing/blue_tote/20180422_ziptab.jpg">
<img src="/test/img/sewing/blue_tote/20180422_zippertab2.jpg">

Add the end tab to the zipper on both sides:
<img src="/test/img/sewing/blue_tote/20180422_zippanel24.jpg">
<img src="/test/img/sewing/blue_tote/20180422_zippertab3.jpg">


Zip panel done:
<img src="/test/img/sewing/blue_tote/20180422_zippanel25.jpg">

So fa we finish 3 parts:
<img src="/test/img/sewing/blue_tote/20180422_3done.jpg">

<h2>Front zipper pocket</h2>

At the back of lining, draw a rectangle and >-----< in the middle of the rectangle:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket1.jpg">

Pin the lining to the front of the front panel
<img src="/test/img/sewing/blue_tote/20180422_frontpocket2.jpg">

Carefully sew around the rectangle:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket2b.jpg">

Carefully cut along >-----< in the middle of the rectangle, dont cut the thread on the rectangle:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket3.jpg">

This is what it should look like:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket4.jpg">

This is what it look like on the other side:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket5.jpg">

Pull the lining through the hole:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket6.jpg">

Iron the edge nicely:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket7.jpg">

Align the zipper in the middle of the hole, and sew it on the lining:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket8.jpg">

This is what it look like at the front:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket9.jpg">

Sew the second piece of pocket lining on the first pocket lining:
<img src="/test/img/sewing/blue_tote/20180422_frontpocket10.jpg">

<img src="/test/img/sewing/blue_tote/20180422_frontpocket11.jpg">
<img src="/test/img/sewing/blue_tote/20180422_frontpocket12.jpg">

So far, we finished these:
<img src="/test/img/sewing/blue_tote/20180422_4done.jpg">

<h2>Side Panels</h2>
Sew elastic pocket on the side panel:
<img src="/test/img/sewing/blue_tote/20180422_sidepanel3.jpg">

Fold and sew two crease like this:
<img src="/test/img/sewing/blue_tote/20180422_sidepanel2.jpg">

<img src="/test/img/sewing/blue_tote/20180422_sidepanel1.jpg">
<img src="/test/img/sewing/blue_tote/20180422_twosidepanels.jpg">

<h2>Construct linings</h2>
Sew front lining from about 3 inches from top, centre:
<img src="/test/img/sewing/blue_tote/20180422_lining1.jpg">
Sew back lining the same way:
<img src="/test/img/sewing/blue_tote/20180422_lining2.jpg">
Sew side panels:
<img src="/test/img/sewing/blue_tote/20180422_lining3.jpg">
<img src="/test/img/sewing/blue_tote/20180422_lining4.jpg">
<img src="/test/img/sewing/blue_tote/20180422_lining5.jpg">
Sew bottom panel, make sure all the corners are overlapped like this:
<img src="/test/img/sewing/blue_tote/20180422_lining6.jpg">
Leave a hole on the bottom panel for turning the bag at last step:
<img src="/test/img/sewing/blue_tote/20180422_lining7.jpg">


<h2>Sew Shoulder Straps</h2>
<img src="/test/img/sewing/blue_tote/20180422_strap1.jpg">
<img src="/test/img/sewing/blue_tote/20180422_strap2.jpg">
<img src="/test/img/sewing/blue_tote/20180422_strap3.jpg">


<h2>Construct outer layers</h2>
<img src="/test/img/sewing/blue_tote/20180422_outer1.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer2.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer3.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer4.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer5.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer6.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer7.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer8.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer9.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer10.jpg">
<img src="/test/img/sewing/blue_tote/20180422_outer11.jpg">

<h2>Sew linings and outer layers together</h2>
<img src="/test/img/sewing/blue_tote/20180422_tog1.jpg">
<img src="/test/img/sewing/blue_tote/20180422_tog2.jpg">
<img src="/test/img/sewing/blue_tote/20180422_tog3.jpg">
<img src="/test/img/sewing/blue_tote/20180422_tog4.jpg">
<img src="/test/img/sewing/blue_tote/20180422_tog5.jpg">
<img src="/test/img/sewing/blue_tote/20180422_tog6.jpg">
<img src="/test/img/sewing/blue_tote/20180422_tog7.jpg">

<h2>At last</h2>

