---
layout: page
title: Hydrangea Tote
date: 2018-06-01
image: /test/img/sewing/hydrangea_tote/20180511_hydrangea1.jpg 
categories: sewing

---
Features: Top zipper, front zipper pocket, side elastic pockets, lining

<h2>Materials</h2>
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangeaM1.jpg">
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangeaM2.jpg">
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangeaM3.jpg">

<h2>The finished tote</h2>
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangea1.jpg">
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangea2.jpg">
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangea3.jpg">
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangea4.jpg">
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangea5.jpg">
<img src="/test/img/sewing/hydrangea_tote/20180511_hydrangea6.jpg">