---
layout: page_recipe
title: Dodo's Swiss Chocolate Cheese Cake
date: 2015-02-21
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 2 package cream cheese
        - 1/2 cup sour cream
        - 4 large eggs 
        - 3/4 cup sugar
        
    wet:
        - 4 dark semi sweet baking chocolate, melted
    
    extra:
        - 1 and 1/4 cup graham crumbs 
        - 1/4 cup butter melted
        
        
    
steps:
    - Prepare crust, keep in fridge.
    - Prepare springform pan, lined with parchment paper.
    - Beat cream cheese and sour cream well until smooth. Beat in sugar. Beat in eggs.
    - Pour 2/3 into the springform pan lined with parchment paper.
    - Add the chocolate mixture into the rest of the batter. Beat well until smooth. 
    - Pour into pan. Swirl slightly with a spatula to create marble effect.
    - Bake at 325F about 50 minutes. 
    
---
