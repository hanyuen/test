---
layout: page_recipe
title: Dodo's Marbled Cake
date: 2015-02-20
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 3 cups flour
        - 4 teaspoons baking powder
        - 1/2 teaspoons salt
        
    wet:
        - 4 large eggs 
        - 1.5 cup sugar (not enough sugar, it will be chewy)
        - 1 cup vegetable oil
        - 1 cup orange juice
        - 3 tablespoons cocoa powder + water
    
    extra:
        
    
steps:
    - Mix dry ingredients in a bowl. (A) 
    - Mix wet ingredients except orange juice in another bowl. Alternatively add (A) and orange juice. Beat well until smooth.
    - Pour half into pan. 
    - Add the cocoa powder mixture into second half of batter. Beat well until smooth. 
    - Pour into pan. slightly mix with a spatula to create marble effect.
    - Bake at 350F about 60 minutes. 
    
---
