---
layout: page_recipe
title: Coconut Cookies
date: 2015-04-21
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 1 cup flour
        - 6 tablespoons vegetable oil
        - 1/4 cup brown sugar
        - 1/2 cup coconut, shredded, unsweetened
    
    wet:
        
    extra:
        - 10 apples, peeled and cored, sliced 
        - 1/2 cup sugar
        - 3 teaspoons cinnamon
        
steps:
    - Mix everything together.
    - Shape into mini muffins pan (24 holes).
    - Bake at 355F about 15 minutes. 
    
---
