---
layout: page_recipe
title: Slow Bake Chocolate Cake
date: 2017-09-15
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 2 cups flour
        - 2 teaspoons baking soda
        - 3/4 cup cocoa powder
        - 1 cup sugar
        - 1/2 teaspoons salt
    wet:
        - 2 large eggs
        - 1 cup buttermilk (1 tablespoon lemon juice + 1 cup milk)
        - 1 cup vegetable oil (NOT olive oil)
        - 1 teaspoon vanilla extract
        - 1 cup boiling water
        
    extra:
        - cream cheese
        - icing sugar
        
    
steps:
    - Place everything except the boiling water in a mixing bowl.
    - Mix at low speed until all mix. This is little thick.
    - Slowly add boiling water to the mixture. Continue to mix until well combined and smooth.
    - Pour into greased 9x9 square baking pan. 
    - Bake at 300F about 60 minutes.
    - Cool completely before put icing.
    - Cut the cake into three slices, put icing in the middle.
    
---
