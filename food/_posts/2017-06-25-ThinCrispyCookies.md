---
layout: page_recipe
title: Thin Crispy Cookies
date: 2017-06-23
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 1/4 cup flour
        
    wet:
        - 1 large eggs 
        - 1 egg white (optional)
        - 1/2 cup sugar (or less)
        - 1 cup pumpkins seeds / sunflower seeds / sesame seeds
    
steps:
    - Cover 10x15 cookies sheet with parchment paper.
    - Mix all ingredients in a bowl. 
    - Spread very thin layer on cookies sheet ... to the extent when you feel its too thin.
    - Bake at 325F about 20 minutes. 
    
---
