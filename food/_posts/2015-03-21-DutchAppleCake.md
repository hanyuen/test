---
layout: page_recipe
title: Dodo's Dutch Apple Cake
date: 2015-03-21
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 4 cups flour
        - 2.5 teaspoons baking powder
    
    wet:
        - 1/2 cup orange juice
        - 1 cup vegetable oil
        - 3 large eggs 
        - 1 cup sugar
    
    extra:
        - 10 apples, peeled and cored, sliced 
        - 1/2 cup sugar
        - 3 teaspoons cinnamon
        
steps:
    - Prepare apples, set a side.
    - Mix dry ingredients in a bowl (A).
    - Mix wet ingredients in a separate bowl. Add (A). Knead to make soft dough.
    - With hands spread half into the 9x12 pan.
    - Add the apples.
    - Spread the rest to cover the apples.
    - Bake at 350F about 50 minutes. 
    
---
