---
layout: page_recipe
title: Simple Oatmeal Cookies
date: 2017-09-21
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 1.5 cups flour
        - 1.5 cup quick oats
        - 0.5 teaspoons baking soda
        - 1/4 teaspoons salt
    wet:
        - 1 large eggs
        - 1 cup brown sugar
        - 0.5 cup butter softened
        - 1/4 cup vegetable oil
        
    
steps:
    - Mix all dry ingredients in a bowl. (A)
    - Mix all wet ingredients low speed until all mix. Add (A) and mix  
    - Roll into ball press down ~2cm thick.
    - Bake at 375F about 12 minutes.
    
---
