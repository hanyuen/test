---
layout: page_recipe
title: Blueberries Loaded Banana Muffins
date: 2017-06-23
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 2 cups flour
        - 2 teaspoons baking powder
        
    wet:
        - 2 large eggs 
        - 3/4 cup sugar
        - 1/2 cup vegetable oil
        - 1/2 cup milk
        - 1 teaspoon vanilla extract
        - 3 bananas ripe, mashed 
    
    extra:
        - frozen wild blueberries        
    
steps:
    - Mix dry ingredients in a bowl. (A) 
    - Mix wet ingredients in a bowl. Add (A). Should be little thick. DO NOT OVERMIX.
    - Add frozen blueberries, slightly mix. DO NOT OVERMIX. 
    - Scoop into muffin cups.
    - Bake at 350F about 20 minutes. 
    
---
