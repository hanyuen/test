---
layout: page_recipe
title: Sponge Cake
date: 2017-06-23
image: /test/img/food/20170718_plumcake.jpg
categories: food

ingredients:
    dry:
        - 1 cup flour
        - 1.5 teaspoons baking powder
    wet:
        - 5 large eggs, separate egg white and yolk
        - 3/4 cup sugar
        - 1/3 cup milk
        - 1/2 teaspoon vanilla extract
        
    
steps:
    - Mix all dry ingredients in a bowl. (A)
    - Beat egg white until peak (B)
    - Mix all wet ingredients medium speed until well mixed, pale color. Add (A) and mix. Fold in (B).
    - Bake at 350F about 30 minutes.
    - Orange Sponge Cake: Add orange rind and some fresh orange juice instead of milk.
    - Green Tea Sponge Cake: Green tea powder and water mix into paste, add to batter.     
    
---
