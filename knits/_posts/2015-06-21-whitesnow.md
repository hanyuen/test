---
layout: page_knit
title: white snow
date: 2015-06-21
image: /test/img/20170805_160006.jpg 
categories: knits
pattern: 
    -cast on:68
    -knit 2, purl 2 for 1.5 inches
    -knit 5 inches in the round
    -divide
    
---

Top-down sweater for Nathan. 


